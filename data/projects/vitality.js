const vitality = {
  "name": "vitality",
  "slug": "vitality",
  "category": "Website",
  "context": {
    1: "As part of the 3rd grade pedagogical project at Hetic School, I had the chance to work on the new Vitality team website, as a <strong>developer</strong>.",
    2: "Team Vitality is an esport team that was created in August 2013. Accompanied by reputable partners and representing the best French players on numerous games, they also shines internationally thanks to their impressive list of achievements."
  },
  "context_fr": {
    1: "Dans le cadre du projet pédagogique de 3ème année de l'école Hetic, j'ai eu la chance de travailler sur le nouveau site web de l'équipe Vitality, en tant que <strong>développeur</strong>.",
    2: "La team Vitality est une équipe esport créée en août 2013. Accompagnés de partenaires réputés et représentant les meilleurs joueurs français sur de nombreux jeux, ils brillent également à l'international grâce à leur impressionnant palmarès."
  },
  "technical": {
    1: "As part of the 3rd grade pedagogical project at Hetic School, I had the chance to work on the new Vitality team website, as a <strong>developer</strong>.",
    2: "Team Vitality is an esport team that was created in August 2013. Accompanied by reputable partners and representing the best French players on numerous games, it also shines internationally thanks to its impressive list of achievements.",
    role: "Developer",
    technos: "Wordpress, NodeJS, Sass, TweenMax, PHP, Gulp",
    year: "2017"
  },
  "technical_fr": {
    1: "As part of the 3rd grade pedagogical project at Hetic School, I had the chance to work on the new Vitality team website, as a <strong>developer</strong>.",
    2: "Team Vitality is an esport team that was created in August 2013. Accompanied by reputable partners and representing the best French players on numerous games, it also shines internationally thanks to its impressive list of achievements.",
    role: "Developer",
    technos: "Wordpress, NodeJS, Sass, TweenMax, PHP, Gulp",
    year: "2017"
  },
  "previous": "Richelieu",
  "next": "Bière à table",
  "mainColor": "pureBlack",
  "secondColor": "#FAAE39",
  "pictures": 4
}

export default vitality
