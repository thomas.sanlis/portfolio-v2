const yogosha = {
  "name": "yogosha",
  "slug": "yogosha",
  "category": "Website",
  "context": {
    1: "As part of the 3rd grade pedagogical project at Hetic School, I had the chance to work on the new Vitality team website, as a <strong>developer</strong>.",
    2: "Team Vitality is an esport team that was created in August 2013. Accompanied by reputable partners and representing the best French players on numerous games, it also shines internationally thanks to its impressive list of achievements."
  },
  "context_fr": {
    1: "As part of the 3rd grade pedagogical project at Hetic School, I had the chance to work on the new Vitality team website, as a <strong>developer</strong>.",
    2: "Team Vitality is an esport team that was created in August 2013. Accompanied by reputable partners and representing the best French players on numerous games, it also shines internationally thanks to its impressive list of achievements."
  },
  "technical": {
    1: "As part of the 3rd grade pedagogical project at Hetic School, I had the chance to work on the new Vitality team website, as a <strong>developer</strong>.",
    2: "Team Vitality is an esport team that was created in August 2013. Accompanied by reputable partners and representing the best French players on numerous games, it also shines internationally thanks to its impressive list of achievements.",
    role: "Developer",
    technos: "Wordpress, NodeJS, Sass, TweenMax, PHP, Gulp",
    year: "2017"
  },
  "technical_fr": {
    1: "As part of the 3rd grade pedagogical project at Hetic School, I had the chance to work on the new Vitality team website, as a <strong>developer</strong>.",
    2: "Team Vitality is an esport team that was created in August 2013. Accompanied by reputable partners and representing the best French players on numerous games, it also shines internationally thanks to its impressive list of achievements.",
    role: "Developer",
    technos: "Wordpress, NodeJS, Sass, TweenMax, PHP, Gulp",
    year: "2017"
  },
  "previous": "Richelieu",
  "next": "Bière à table",
  "mainColor": "yogosha",
  "pictures": 4
}

export default yogosha
