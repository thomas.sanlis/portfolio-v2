import vitality from './projects/vitality'
import yogosha from './projects/yogosha'
import cta from './projects/cta'
import scribe from './projects/scribe'

const projects = {
  scribe,
  cta,
  yogosha,
  vitality,
}

export default projects
