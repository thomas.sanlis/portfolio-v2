---
title: Allô le monde
slug: article-1
desc: my personal introduction to the world of words on the web
date: January 9, 2018
---

<image-comp source="avatar.png"
            alt="test lol"
            width="'auto'"
            height="'auto'" />

# Build an Intelligent Personal Assistant for your Business

## Are you interested in any of the following for your business?

1. Reaching **more customers** than ever before with **open rates of 80-90%+** 📈
2. **Automatically** keeping your customers **highly engaged** through personal messaging
   - For example
3. Providing an **"app"** like experience without them having to download anything 📱
   - **Custom UI** specifically **tailored** for your business products and services

## Yes I'm interested, how is this possible? 🤨

![](https://cdn-images-1.medium.com/max/1000/1*ZFau16vpEm1F6uzXprgDEQ.png)

## Filter

La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui remplissent une condition déterminée par la fonction callback.

    // 1. Filter the list of inventors for those who were born in the 1500's
    const fifteen = inventors.filter( inventor => inventor.year > 1499 && inventor.year < 1600 )
