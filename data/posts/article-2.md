---
title: hi, world
slug: article-2
desc: my personal introduction to the world of words on the web
date: January 9, 2018
---

hi, world! this is my first blog post. i'm hoping to use this as part creative outlet, part
self-encouragement to see technical projects through, and part place to document my learning so i
can go back and look at all the things i forgot, one day.

to those of you who don't know me, here's a brief introduction: my name is mat! i'm a communist,
a [partner][1], and generally (at the very least) something of a curmudgeon. i have a number of
strong opinions, especially about technology and politics. i have a love for art, and, in another
life, i really wish that i'd studied philosophy or music.

if you have ideas for me to blog about, feel free to reach out to me at mess [at] yelp [dot] com.

[1]: https://macklovesmatt.com/ 'me and my partner'
