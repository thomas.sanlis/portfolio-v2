import Vuex from 'vuex'

const store = () => {
  return new Vuex.Store({
    state: () => ({
      isDark: false,
      currentProject: false,
    })
  })
}

export default store
