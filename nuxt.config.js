const path = require('path')
import blog from './data/blog.js'

import {
  I18N
} from './config'

module.exports = {

  head: {
    titleTemplate: '%s | Thomas Sanlis',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'thomas sanlis portfolio'
      }
    ]
  },

  webfontloader: {
    google: {
      families: ['Work+Sans:300,400,600']
    }
  },

  css: [
    '@/assets/css/tailwindBase.css',
    '@/assets/css/main.scss',
    '@/assets/css/tailwindUtilities.css'
  ],

  router: {
    base: '',
    linkActiveClass: 'is-active',
    scrollBehavior: function() {
      return {
        x: 0,
        y: 0
      }
    }
  },

  plugins: [{
      src: '~/plugins/anime.js',
      ssr: false
    },
    '~/plugins/highlight.js',
    '~/plugins/image.js',
    '~/plugins/code.js',
    '~/plugins/scroll.js'
  ],

  loading: {
    color: '#3B8070'
  },

  modules: [
    '@nuxtjs/pwa',
    'nuxt-webfontloader',
    ['nuxt-i18n', I18N]
  ],

  generate: {
    routes: []
      .concat(blog.map(slug => `/blog/${slug}`))
  },

  build: {
    extractCSS: true,
    postcss: {
      plugins: {
        tailwindcss: path.resolve('./tailwind.config.js')
      }
    },
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      config.module.rules.push({
        test: /\.md$/,
        loader: 'frontmatter-markdown-loader',
        include: path.resolve(__dirname, 'data'),
        options: {
          vue: {
            root: "dynamicMarkdown"
          }
        }
      })
    }
  }
}
