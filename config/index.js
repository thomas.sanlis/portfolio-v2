export const en = require('../locales/en.js')
export const fr = require('../locales/fr.js')

export const I18N = {
  locales: [{
      code: 'en',
      file: 'en.js',
      iso: 'en-US',
      name: 'English'
    },
    {
      code: 'fr',
      file: 'fr.js',
      iso: 'fr-FR',
      name: 'Français'
    }
  ],
  defaultLocale: 'en',
  lazy: true,
  langDir: 'locales/',
  parsePages: false,
  vueI18n: {
    fallbackLocale: 'en',
    messages: {
      en,
      fr
    }
  }
}

export default {
  I18N
}
