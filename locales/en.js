export default {
  hero: {
    title: "Hi! I'm Thomas",
    subtitle: "Freelance and student web developper",
    button: "See my work 👇"
  },
  work: {
    title: "My latest projects",
    subtitle: "Look at them all!"
  }
}
