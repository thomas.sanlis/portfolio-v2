export default {
  hero: {
    title: "Salut ! Je suis Thomas",
    subtitle: "Freelance et étudiant web développeur",
    button: "Voir mes réalisations 👇"
  },
  work: {
    title: "Mes derniers projets",
    subtitle: "Regardez-les tous !"
  }
}
